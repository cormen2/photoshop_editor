#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QVBoxLayout>
#include <QPushButton>
#include "BasicOperations/Edge/canny.h"

#include <QGraphicsView>
#include <QGraphicsScene>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    //------------------------------------------------------

    mainTree = new MainTree(this);
    this->addDockWidget(Qt::LeftDockWidgetArea,mainTree->doc);

    mainSetting = new MainSetting;
    this->addDockWidget(Qt::LeftDockWidgetArea,mainSetting->doc);

    graphicScene = new CustomGraphicsView(this);
    graphicView = new QGraphicsView(graphicScene);
    graphicView->setAlignment(Qt::AlignCenter);
    setCentralWidget(graphicView);

    graphicScene->menu = new QMenu;
    connect(graphicScene->menu, SIGNAL(triggered(QAction*)), this, SLOT(cropReciever(QAction*)), Qt::UniqueConnection);
    connect(mainTree,SIGNAL(sendIndex(int)),this,SLOT(test(int)));

}



MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::imShow(QImage image)
{

    graphicScene->clear();
    item  = new QGraphicsPixmapItem(QPixmap::fromImage(image));
    graphicScene->addItem(item);

    graphicView->show();
}

void MainWindow::test(int i)
{
    qDebug()  << QString::number(i);
}

void MainWindow::treeSignalReciever(QTreeWidgetItem* treeItem ,int index)
{

    QLayoutItem *child;
    while ((child = mainSetting->mainVLayout->takeAt(0)) != 0) {
        delete child->widget();
        delete child;
    }

    switch (treeItem->text(1).toInt()) {
    case CANNY:
       {

        canny = new Canny;
        mainSetting->mainVLayout->addWidget(canny);
        connect(canny,SIGNAL(cannyEcecute(int ,int)),this,SLOT(cannyExecuteReciever(int , int)));

        break;
       }
    case SOBEL:
    {
        sobel = new Sobel;
        mainSetting->mainVLayout->addWidget(sobel);
        connect(sobel,SIGNAL(sobelEcecute(int,int,int)),this,SLOT(sobelExecuteReciever(int,int,int)));

        break;
    }

    case LAPLACIAN:
    {
        laplacian = new Laplacian;
        mainSetting->mainVLayout->addWidget(laplacian);
        connect(laplacian,SIGNAL(laplacianEcecute(int, int)),this,SLOT(laplacianExecuteReciever(int, int)));

        break;
    }
    case EROSION:
    {
        er = new Erosion;
        mainSetting->mainVLayout->addWidget(er);
        connect(er,SIGNAL(run()),this,SLOT(erodeExecuteReciever()));

        break;
    }
    case DILATION:
    {
        di = new Dilation;
        mainSetting->mainVLayout->addWidget(di);
        connect(di,SIGNAL(run()),this,SLOT(dilateExecuteReciever()));
        break;
    }
    case ADVTRANS:
    {
        advt = new ADVTransformation;
        mainSetting->mainVLayout->addWidget(advt);
        connect(advt,SIGNAL(run()),this,SLOT(AdvTExecuteReciever()));
        break;
    }
    case THINING:
    {
        thin = new Thinning;
        mainSetting->mainVLayout->addWidget(thin);
        connect(thin,SIGNAL(run()),this,SLOT(thiningExecuteReciever()));
        break;
    }
    case SHOW_HISTOGRAM:
    {
        histShow = new ShowHistogram;
        mainSetting->mainVLayout->addWidget(histShow);
        connect(histShow,SIGNAL(histogramShowEcecute()),this,SLOT(showHistogramExecuteReciever()));
        break;
    }
    case HISTOGRAM_EQUALIZATION:
    {
        histEqualization = new HistogramEqualization;
        mainSetting->mainVLayout->addWidget(histEqualization);
        connect(histEqualization,SIGNAL(histogramEqualizationEcecute()),this,SLOT(histogramEqualizationExecuteReciever()));
        break;
    }
    case LAPLACIAN_SHARP:
    {
        lapSh = new LaplacianSharp;
        mainSetting->mainVLayout->addWidget(lapSh);
        connect(lapSh,SIGNAL(run()),this,SLOT(LaplacianSharpExecuteReciever()));
        break;
    }
    case GUSSIAN:
    {
        gauss = new Gaussian;
        mainSetting->mainVLayout->addWidget(gauss);
        connect(gauss,SIGNAL(run()),this,SLOT(GaussianExecuteReciever()));
        break;
    }
    case MEDIAN:
    {
        med = new Median;
        mainSetting->mainVLayout->addWidget(med);
        connect(med,SIGNAL(run()),this,SLOT(MedianExecuteReciever()));
        break;
    }
    case BILATERAL:
    {
        bilat = new Bilateral;
        mainSetting->mainVLayout->addWidget(bilat);
        connect(bilat,SIGNAL(run()),this,SLOT(BilateralExecuteReciever()));
        break;
    }
    case CUSTOM:
    {
        custom = new CustomFunction;
        mainSetting->mainVLayout->addWidget(custom);
        connect(custom,SIGNAL(run()),this,SLOT(CustomeFuncExecuteReciever()));
        break;
    }
    default:

        qDebug()  << treeItem->text(1);
        break;
    }
}

void MainWindow::cannyExecuteReciever(int threshold1, int threshold2)
{
   cv::Mat edgesMat = canny->execute(QImage2Mat(mainImage), threshold1, threshold2);
   QImage edges = Mat2QImage(edgesMat);
   imShow(edges);
}

void MainWindow::erodeExecuteReciever()
{
    QPixmap pixMap = graphicView->grab();
    cv::Mat image = QImage2Mat(pixMap.toImage());

    er->execute(&image);

    QImage qimage = Mat2QImage(image);

    imShow(qimage);
}

void MainWindow::AdvTExecuteReciever()
{
    QPixmap  pixMap = graphicView->grab();
    cv::Mat image = QImage2Mat(pixMap.toImage());
    advt->execute(&image);

    QImage qimage = Mat2QImage(image);

    imShow(qimage);
}

void MainWindow::dilateExecuteReciever()
{
    QPixmap  pixMap = graphicView->grab();
    cv::Mat image = QImage2Mat(pixMap.toImage());
    di->execute(&image);

   QImage qimage = Mat2QImage(image);

   imShow(qimage);
}

void MainWindow::sobelExecuteReciever(int alpha , int beta, int gamma)
{
   cv::Mat edgesMat = sobel->execute(QImage2Mat(mainImage), alpha, beta, gamma);
   QImage edges = Mat2QImage(edgesMat);
   imShow(edges);
}

void MainWindow::laplacianExecuteReciever(int alpha , int beta)
{
    cv::Mat edgesMat = laplacian->execute(QImage2Mat(mainImage), alpha , beta);
    QImage qimage = Mat2QImage(edgesMat);
    imShow(qimage);
}

void MainWindow::thiningExecuteReciever()
{
    QPixmap  pixMap = graphicView->grab();
    cv::Mat image = QImage2Mat(pixMap.toImage());
    thin->execute(&image);

    QImage qimage = Mat2QImage(image);

    imShow(qimage);
}

void MainWindow::LaplacianSharpExecuteReciever()
{
    QPixmap pixMap = graphicView->grab();
    cv::Mat image = QImage2Mat(pixMap.toImage());

    lapSh->execute(&image);
    imShow(Mat2QImage(image));
}

void MainWindow::showHistogramExecuteReciever()
{
    QPixmap  pixMap = graphicView->grab();
    cv::Mat image = QImage2Mat(pixMap.toImage());

    cv::Mat outPut = histShow->viewHistograms(image );
    QImage qimage = Mat2QImage(outPut);

    imShow(qimage);
}

void MainWindow::histogramEqualizationExecuteReciever()
{
    QPixmap  pixMap = graphicView->grab();
    cv::Mat image = QImage2Mat(pixMap.toImage());
    cv::Mat outPut = histEqualization->histEq( image );

     QImage qimage = Mat2QImage(outPut);

     imShow(qimage);
}

void MainWindow::GaussianExecuteReciever()
{
    QPixmap pixMap = graphicView->grab();
    cv::Mat image = QImage2Mat(pixMap.toImage());
    gauss->execute(&image);
    imShow(Mat2QImage(image));
}

void MainWindow::MedianExecuteReciever()
{
    QPixmap pixMap = graphicView->grab();
    cv::Mat image = QImage2Mat(pixMap.toImage());
    med->execute(&image);
    imShow(Mat2QImage(image));
}

void MainWindow::BilateralExecuteReciever()
{
    QPixmap pixMap = graphicView->grab();
    cv::Mat image = QImage2Mat(pixMap.toImage());
    bilat->execute(&image);
    imShow(Mat2QImage(image));
}

void MainWindow::CustomeFuncExecuteReciever()
{
    QPixmap pixMap = graphicView->grab();
    cv::Mat image = QImage2Mat(pixMap.toImage());
    custom->execute(&image);
    imShow(Mat2QImage(image));
}

void MainWindow::on_actionOpen_triggered()
{
    QString fname = QFileDialog::getOpenFileName(this, "Open", qApp->applicationDirPath(), "Images (*.jpg *.bmp *.tif *.png)");
       if(!QFile(fname).exists())
           return;

       mainImage = QImage(fname);

       imShow(mainImage);

}

void MainWindow::on_actionReset_Image_triggered()
{

    graphicScene->clear();
    imShow(mainImage);
    graphicScene->cropRectIsDrawed = false;
}

QImage MainWindow::Mat2QImage(cv::Mat const& src)
{


    QImage dest(src.cols, src.rows, QImage::Format_ARGB32);

      const float scale = 255.0;

      if (src.depth() == CV_8U) {
        if (src.channels() == 1) {
          for (int i = 0; i < src.rows; ++i) {
            for (int j = 0; j < src.cols; ++j) {
              int level = src.at<quint8>(i, j);
              dest.setPixel(j, i, qRgb(level, level, level));
            }
          }
        } else if (src.channels() == 3) {
          for (int i = 0; i < src.rows; ++i) {
            for (int j = 0; j < src.cols; ++j) {
              cv::Vec3b bgr = src.at<cv::Vec3b>(i, j);
              dest.setPixel(j, i, qRgb(bgr[2], bgr[1], bgr[0]));
            }
          }
        }
      } else if (src.depth() == CV_32F) {
        if (src.channels() == 1) {
          for (int i = 0; i < src.rows; ++i) {
            for (int j = 0; j < src.cols; ++j) {
              int level = scale * src.at<float>(i, j);
              dest.setPixel(j, i, qRgb(level, level, level));
            }
          }
        } else if (src.channels() == 3) {
          for (int i = 0; i < src.rows; ++i) {
            for (int j = 0; j < src.cols; ++j) {
              cv::Vec3f bgr = scale * src.at<cv::Vec3f>(i, j);
              dest.setPixel(j, i, qRgb(bgr[2], bgr[1], bgr[0]));
            }
          }
        }
      }

      return dest;
}

cv::Mat MainWindow::QImage2Mat(QImage const& src)

{
    cv::Mat mat = cv::Mat(src.height(), src.width(), CV_8UC4, (uchar*)src.bits(), src.bytesPerLine());
    cv::Mat mat2 = cv::Mat(mat.rows, mat.cols, CV_8UC3 );
    int from_to[] = { 0,0, 1,1, 2,2 };
    cv::mixChannels( &mat, 1, &mat2, 1, from_to, 3 );



    return mat2;
}


void MainWindow::on_actionCrop_triggered()
{

    if(graphicScene->cropIsSelected == true){

        graphicScene->cropIsSelected = false;

    }else{

        graphicScene->cropIsSelected = true;

    }
    qDebug() << isCropClicked;
}

void MainWindow::on_label_main_clicked()
{

}

void MainWindow::on_actionDone_triggered()
{

//    int x = (graphicView->width() - graphicScene->width())/2;
//    int y = (graphicView->height() - graphicScene->height())/2;

//    QPoint topLeftMain(x, y);
//    QPoint bottomRightMain(mainImage.size().width()+x, mainImage.size().height()+y);
//    QRect rectMain = QRect(topLeftMain, bottomRightMain);

    QPixmap pixMap = item->pixmap();
    QImage  image = pixMap.toImage();

    QPoint topLeft(graphicScene->startX, graphicScene->startY);
    QPoint bottomRight(graphicScene->endX, graphicScene->endY);

    QRect rect = QRect(topLeft, bottomRight);
    QImage cropped = image.copy(rect);

    graphicScene->clear();

    imShow(cropped);



}

void MainWindow::cropReciever(QAction * action)
{

    if(action->data().toInt()==1){

        qDebug()<< "clicked on crop";
        QPixmap pixMap = item->pixmap();
        QImage  image = pixMap.toImage();

        QPoint topLeft(graphicScene->startX, graphicScene->startY);
        QPoint bottomRight(graphicScene->endX, graphicScene->endY);

        QRect rect = QRect(topLeft, bottomRight);
        QImage cropped = image.copy(rect);

        graphicScene->clear();

        imShow(cropped);

        graphicScene->cropRectIsDrawed = false;

    }else if(action->data().toInt()==2){

        qDebug()<< "clicked on cancel";
        graphicScene->clear();
        imShow(mainImage);
        graphicScene->cropRectIsDrawed = false;
    }
}
