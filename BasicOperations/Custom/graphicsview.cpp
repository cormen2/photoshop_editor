#include "graphicsview.h"
#include <QDebug>
#include <QGraphicsSceneMouseEvent>
#include <QRect>
#include <QRectF>
#include "myrect.h"

GraphicsView::GraphicsView(QWidget *parent) :
    QGraphicsView(parent)
{
    this->setRenderHint(QPainter::Antialiasing);
    this->setCacheMode(QGraphicsView::CacheBackground);
    this->setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);

    this->setFixedSize(270,270);

    scene = new QGraphicsScene;
    this->setScene(scene);
    myRect *start = new myRect(QPointF(0,255));
    myRect *end = new myRect(QPointF(255,0));
    points.append(start);
    points.append(end);
    drowCurve();

    for (int i = 0; i < points.size(); ++i)
    {
        scene->addItem(points[i]);
    }

}

void GraphicsView::mouseDoubleClickEvent(QMouseEvent *mouseEvent)
{
    if (mouseEvent->button() == Qt::LeftButton)
    {
        QPoint pos = mouseEvent->pos();
        if(pos.x() >=0 && pos.x()<=255 && pos.y()>=0 && pos.y()<=255)
        {
            myRect *myrec = new myRect(QPointF(pos.x(),pos.y()));
            points.append(myrec);

            scene->addItem(points[points.size()-1]);
            connect(myrec,SIGNAL(changeRectPose()),this,SLOT(changePos()));

            qSort(points.begin(),points.end(),
                  [](const myRect* first, const myRect* second)
            { return first->pos().x()<(second->pos().x()); });

            drowCurve();
        }
    }
}

void GraphicsView::changePos()
{
    qSort(points.begin(),points.end(),
          [](const myRect* first, const myRect* second)
    { return first->pos().x()<(second->pos().x()); });

    drowCurve();
}

void GraphicsView::drowCurve()
{
    curve = PathToCurve(points,1,40);
    foreach (QGraphicsLineItem *i, lines) {
        scene->removeItem(i);
    }
    lines.clear();
    for(int i=0; i<curve.size()-1; ++i)
    {
        QGraphicsLineItem *li = scene->addLine(QLineF(curve[i].first,curve[i].second,curve[i+1].first,curve[i+1].second),QPen(QColor(255,0,0)));
        lines.append(li);
    }
}


QPair<double, double> GraphicsView::CalCurveInt(double t, QPair<double, double> p1, QPair<double, double> p2, QPair<double, double> p3, QPair<double, double> p4 )
{
    double c1,c2,c3,c4;

    c1 = M12*p2.first;
    c2 = M21*p1.first + M23*p3.first;
    c3 = M31*p1.first + M32*p2.first + M33*p3.first + M34*p4.first;
    c4 = M41*p1.first + M42*p2.first + M43*p3.first + M44*p4.first;

    double x = (((c4*t + c3)*t +c2)*t + c1);

    c1 = M12*p2.second;
    c2 = M21*p1.second + M23*p3.second;
    c3 = M31*p1.second + M32*p2.second + M33*p3.second + M34*p4.second;
    c4 = M41*p1.second + M42*p2.second + M43*p3.second + M44*p4.second;

    double y = (((c4*t + c3)*t +c2)*t + c1);

    return qMakePair( x, y );
}
QVector< QPair<double, double> > GraphicsView::PathToCurve(QVector< myRect* > PaPath, int step, int section)
{
    QVector< QPair<double , double > > curvePath;

    if(PaPath.size() == 0)
        return curvePath;

    QPair<double , double > p;
    int v1,v2,v3,v4;
    for(int i=0; i<PaPath.size(); i=i+step)
    {
        v1 = i-step;
        v2 = i;
        v3 = i+step;
        v4 = i+step*2;

        if( i-step < 0 )
            v1=0;
        if( PaPath.size() <= i+step )
            v3=PaPath.size()-1;
        if( PaPath.size() <= i+step*2 )
            v4=PaPath.size()-1;

        //printf("[%d] - %d %d %d %d\n",PaPath.size(), v1, v2, v3, v4);

        float V = (PaPath[v2]->pos().x()-PaPath[v3]->pos().x())*(PaPath[v2]->pos().x()-PaPath[v3]->pos().x()) +(PaPath[v2]->pos().y()-PaPath[v3]->pos().y())*(PaPath[v2]->pos().y()-PaPath[v3]->pos().y());
        double dist = sqrt( V );

        double eT = dist/section;
        //printf("dist = %lf, step = %lf \n", dist, eT);

        for(double t=0; t<dist; t+=eT)
        {
            p = CalCurveInt( t/dist,QPair<double,double>(PaPath[v1]->pos().x(),PaPath[v1]->pos().y()),
                             QPair<double,double>(PaPath[v2]->pos().x(),PaPath[v2]->pos().y()),
                             QPair<double,double>(PaPath[v3]->pos().x(),PaPath[v3]->pos().y()),
                             QPair<double,double>(PaPath[v4]->pos().x(),PaPath[v4]->pos().y()));
            if(p.first < 0)
                p.first = 0;
            if(p.first > 255)
                p.first = 255;
            if(p.second < 0)
                p.second= 0;
            if(p.second > 255)
                p.second = 255;

            curvePath.push_back(p);
        }
    }

    curvePath.push_back(QPair<double,double>( PaPath[PaPath.size()-1]->pos().x(), PaPath[PaPath.size()-1]->pos().y()));

    return curvePath;
}




