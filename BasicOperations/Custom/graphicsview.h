#ifndef GRAPHICSVIEW_H
#define GRAPHICSVIEW_H

#include <QGraphicsScene>
#include <QStack>
#include <QPair>
#include <QMouseEvent>

#define M11  0.0
#define M12  1.0
#define M13  0.0
#define M14  0.0
#define M21 -0.5
#define M22  0.0
#define M23  0.5
#define M24  0.0
#define M31  1.0
#define M32 -2.5
#define M33  2.0
#define M34 -0.5
#define M41 -0.5
#define M42  1.5
#define M43 -1.5
#define M44  0.5
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QLineF>
#include <QPointF>
#include <QGraphicsLineItem>
#include <QPen>
#include <QDebug>
#include <QWidget>
#include <QRect>
#include "myrect.h"


class GraphicsView : public QGraphicsView
{
    Q_OBJECT
public:
    GraphicsView(QWidget *parent = 0);
    QVector<QPair<double,double>> curve;

protected:
    QGraphicsScene *scene;
    void virtual mouseDoubleClickEvent(QMouseEvent *event);
    void drowCurve();
    QPair<double, double> CalCurveInt(double t, QPair<double, double> p1, QPair<double, double> p2, QPair<double, double> p3, QPair<double, double> p4 );
    QVector< QPair<double, double> > PathToCurve(QVector< myRect* > PaPath, int step, int section);

private:
    QVector<myRect*> points;
     QVector<QGraphicsLineItem*> lines;

public slots:
    void changePos();
};

#endif // GRAPHICSSCENE_H
