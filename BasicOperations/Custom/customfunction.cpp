#include "customfunction.h"
#include "ui_customfunction.h"

#include <QDebug>

CustomFunction::CustomFunction(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CustomFunction)
{
    ui->setupUi(this);
    graphic = new GraphicsView;
    ui->layGraphics->addWidget(graphic);
}

CustomFunction::~CustomFunction()
{
    delete ui;
}

void CustomFunction::execute(cv::Mat *image)
{    
    cv::Mat hsvImage;
    cv::cvtColor(*image,hsvImage,CV_BGR2HSV);
    cv::Mat channelsHSV[3];
    cv::split(hsvImage,channelsHSV);

    QVector<QPair<double,double>> curve = graphic->curve;
    cv::Mat lookUp(1, 256, CV_8UC1);
    int arr[256] = {0};
    for (int i = 0; i < 256; ++i) {
        int idx = int((i * curve.length()) / 256);
        lookUp.at<uchar>(i) = uchar(255 - curve[idx].second);
    }

    cv::LUT(channelsHSV[2],lookUp,channelsHSV[2]);
    cv::merge(channelsHSV,3,hsvImage);
    cv::cvtColor(hsvImage,*image,cv::COLOR_HSV2BGR);
}

void CustomFunction::on_btnRun_clicked()
{
    emit run();
}
