#ifndef MYRECT_H
#define MYRECT_H

#include <QObject>
#include <QGraphicsItem>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>
#include <QDebug>
#include <QCursor>
#include <QPointF>

class myRect : public QObject, public QGraphicsItem
{
    Q_OBJECT
public:
    myRect(QPointF,QObject *parent = 0);
    QPointF po;

    ~myRect();

private:
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

signals:
    void changeRectPose();
};

#endif // MYRECT_H
