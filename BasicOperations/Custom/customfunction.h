#ifndef CUSTOMFUNCTION_H
#define CUSTOMFUNCTION_H

#include <QWidget>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include "graphicsview.h"

namespace Ui {
class CustomFunction;
}

class CustomFunction : public QWidget
{
    Q_OBJECT

public:
    explicit CustomFunction(QWidget *parent = 0);
    ~CustomFunction();
    void execute(cv::Mat*);
    GraphicsView *graphic;

private slots:
    void on_btnRun_clicked();

signals:
    void run();

private:
    Ui::CustomFunction *ui;
};

#endif // CUSTOMFUNCTION_H
