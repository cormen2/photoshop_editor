HEADERS += \
    $$PWD/myrect.h \
    $$PWD/graphicsview.h \
    $$PWD/customfunction.h

SOURCES += \
    $$PWD/myrect.cpp \
    $$PWD/graphicsview.cpp \
    $$PWD/customfunction.cpp

FORMS += \
    $$PWD/customfunction.ui
