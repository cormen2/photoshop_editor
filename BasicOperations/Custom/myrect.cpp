#include "myrect.h"

myRect::myRect(QPointF point,QObject *parent) :
    QObject(parent), QGraphicsItem()
{
    this->setPos(point);
    po = point;
}

myRect::~myRect()
{

}

QRectF myRect::boundingRect() const
{
    return QRectF (-5,-5,10,10);
}

void myRect::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setPen(Qt::black);
    painter->setBrush(Qt::green);
    painter->drawRect(-5,-5,10,10);
    Q_UNUSED(option);
    Q_UNUSED(widget);
}

void myRect::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QPointF f = event->scenePos();
    if(f.x() >= 0 && f.x() <= 255 && f.y() >= 0 && f.y() <= 255)
    {
        this->setPos(mapToScene(event->pos()));
        emit changeRectPose();
    }
}

void myRect::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    this->setCursor(QCursor(Qt::ClosedHandCursor));
    Q_UNUSED(event);
}

void myRect::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    this->setCursor(QCursor(Qt::ArrowCursor));
        Q_UNUSED(event);
}
