#ifndef ADVTRANSFORMATION_H
#define ADVTRANSFORMATION_H

#include <QWidget>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

namespace Ui {
class ADVTransformation;
}

class ADVTransformation : public QWidget
{
    Q_OBJECT

public:
    explicit ADVTransformation(QWidget *parent = 0);
    ~ADVTransformation();
    void execute(cv::Mat*);

signals:
    void run();

private slots:
    void on_btnRun_clicked();

private:
    Ui::ADVTransformation *ui;
    int morph_size;
    int morph_elemt;
    int morph_option;

};

#endif // ADVTRANSFORMATION_H
