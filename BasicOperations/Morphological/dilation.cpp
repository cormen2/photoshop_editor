#include "dilation.h"
#include "ui_dilation.h"

Dilation::Dilation(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Dilation)
{
    ui->setupUi(this);
    ui->rdbRECT->setChecked(true);
}

Dilation::~Dilation()
{
    delete ui;
}

void Dilation::execute(cv::Mat * image)
{
    dilate_size = ui->spinSize->value();
    if(ui->rdbRECT->isChecked())
        dilate_type = 0;
    else if(ui->rdbCROSS->isChecked())
        dilate_type = 1;
    else
        dilate_type = 2;

    cv::Mat element = cv::getStructuringElement(dilate_type,
                                                cv::Size(2*dilate_size+1,2*dilate_size+1),
                                                cv::Point(dilate_size,dilate_size));

    cv::dilate(*image,*image,element);
}

void Dilation::on_btnRun_clicked()
{
    emit run();
}
