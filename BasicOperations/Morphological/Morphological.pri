FORMS += \
    $$PWD/erosion.ui \
    $$PWD/dilation.ui \
    $$PWD/advtransformation.ui \
    $$PWD/thinning.ui

HEADERS += \
    $$PWD/erosion.h \
    $$PWD/dilation.h \
    $$PWD/advtransformation.h \
    $$PWD/thinning.h

SOURCES += \
    $$PWD/erosion.cpp \
    $$PWD/dilation.cpp \
    $$PWD/advtransformation.cpp \
    $$PWD/thinning.cpp
