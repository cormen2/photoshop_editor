#include "erosion.h"
#include "ui_erosion.h"

Erosion::Erosion(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Erosion)
{
    ui->setupUi(this);
    ui->rdbRECT->setChecked(true);
}

Erosion::~Erosion()
{
    delete ui;
}

void Erosion::execute(cv::Mat* image)
{
    erode_size = ui->spinSize->value();
    if(ui->rdbRECT->isChecked())
        erode_type = 0;
    else if(ui->rdbCROSS->isChecked())
        erode_type = 1;
    else
        erode_type = 2;

    cv::Mat element = cv::getStructuringElement(erode_type,
                                                cv::Size(2*erode_size+1,2*erode_size+1),
                                                cv::Point(erode_size,erode_size));

    cv::erode(*image,*image,element);
}


void Erosion::on_btnRun_clicked()
{
    emit run();
}
