#ifndef DILATION_H
#define DILATION_H

#include <QWidget>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

namespace Ui {
class Dilation;
}

class Dilation : public QWidget
{
    Q_OBJECT

public:
    explicit Dilation(QWidget *parent = 0);
    ~Dilation();
    void execute(cv::Mat*);

private slots:
    void on_btnRun_clicked();

signals:
    void run();

private:
    Ui::Dilation *ui;
    int dilate_type;
    int dilate_size;
};

#endif // DILATION_H
