#include "advtransformation.h"
#include "ui_advtransformation.h"

ADVTransformation::ADVTransformation(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ADVTransformation)
{
    ui->setupUi(this);
    ui->grbMethod->setLayout(ui->layoutMethods);
    ui->grbElement->setLayout(ui->layoutElement);
    ui->rdbRECT->setChecked(true);
    ui->rdbOpen->setChecked(true);
}

ADVTransformation::~ADVTransformation()
{
    delete ui;
}

void ADVTransformation::execute(cv::Mat * image)
{

    morph_size = ui->spinSize->value();

    if(ui->rdbRECT->isChecked())
        morph_elemt = 0;
    else if(ui->rdbCROSS->isChecked())
        morph_elemt = 1;
    else
        morph_elemt = 2;

    if(ui->rdbOpen->isChecked())
        morph_option = 2;
    else if(ui->rdbClose->isChecked())
        morph_option = 3;
    else if(ui->rdbGradient->isChecked())
        morph_option = 4;
    else if(ui->rdbTophat->isChecked())
        morph_option = 5;
    else if(ui->rdbBlackhat->isChecked())
        morph_option = 6;


    cv::Mat elemnt = cv::getStructuringElement(morph_elemt,
                                               cv::Size(2*morph_size+1,2*morph_size+1),
                                               cv::Point(morph_size, morph_size));

    cv::morphologyEx(*image,*image,morph_option,elemnt);
}

void ADVTransformation::on_btnRun_clicked()
{
    emit run();
}
