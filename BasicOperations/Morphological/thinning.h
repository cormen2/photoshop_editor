#ifndef THINNING_H
#define THINNING_H

#include <QWidget>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

namespace Ui {
class Thinning;
}

class Thinning : public QWidget
{
    Q_OBJECT

public:
    explicit Thinning(QWidget *parent = 0);
    ~Thinning();
    void execute(cv::Mat*);

private:
    Ui::Thinning *ui;
    int morph_size;
    int morph_elemt;
    void ThinSubiteration1(cv::Mat & pSrc, cv::Mat & pDst);
    void ThinSubiteration2(cv::Mat & pSrc, cv::Mat & pDst);
    cv::Mat normalizeLetter(cv::Mat & inputarray, cv::Mat & outputarray) ;


signals:
    void run();
private slots:
    void on_btnRun_clicked();
};

#endif // THINNING_H
