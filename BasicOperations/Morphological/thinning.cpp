#include "thinning.h"
#include "ui_thinning.h"

Thinning::Thinning(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Thinning)
{
    ui->setupUi(this);
}

Thinning::~Thinning()
{
    delete ui;
}

void Thinning::execute(cv::Mat *image)
{
//    morph_size = 1;
//    morph_elemt = 2;

//    cv::Mat elemnt = cv::getStructuringElement(morph_elemt,
//                                               cv::Size(2*morph_size+1,2*morph_size+1),
//                                               cv::Point(morph_size, morph_size));

//    cv::Mat output;
//    cv::Mat input(image->rows,image->cols,CV_8UC1);


//   cv::cvtColor(*image,input,CV_RGB2GRAY);
//   cv::threshold(input,input,127,255,CV_8UC1);

//   cv::morphologyEx(input,output,cv::MORPH_HITMISS,elemnt);

////    *image = input - output;
//   *image = output;



//cv::Mat output;
//*image = normalizeLetter(*image, output);



    //------------
    cv::Mat img(image->rows,image->cols, CV_8UC1);
    cv::cvtColor(*image,img,CV_RGB2GRAY);
    cv::threshold(img, img, 127, 255, cv::THRESH_BINARY);
    cv::Mat skel(img.size(), CV_8UC1, cv::Scalar(0));
    cv::Mat temp(img.size(), CV_8UC1);

//    cv::Mat element = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(3, 3));

        morph_size = 1;
        morph_elemt = 2;
        cv::Mat element = cv::getStructuringElement(morph_elemt,
                                                   cv::Size(2*morph_size+1,2*morph_size+1),
                                                   cv::Point(morph_size, morph_size));

    bool done;
    do
    {
      cv::morphologyEx(img, temp, cv::MORPH_OPEN, element);
      cv::bitwise_not(temp, temp);
      cv::bitwise_and(img, temp, temp);
      cv::bitwise_or(skel, temp, skel);
      cv::erode(img, img, element);

      double max;
      cv::minMaxLoc(img, 0, &max);
      done = (max == 0);
    } while (!done);

    *image = skel;


    //------------------

}



void Thinning::on_btnRun_clicked()
{
    emit run();
}

