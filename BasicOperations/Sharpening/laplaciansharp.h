#ifndef LAPLACIANSHARP_H
#define LAPLACIANSHARP_H

#include <QWidget>
#include <opencv2/imgproc.hpp>
#include <opencv2/core.hpp>

namespace Ui {
class LaplacianSharp;
}

class LaplacianSharp : public QWidget
{
    Q_OBJECT

public:
    explicit LaplacianSharp(QWidget *parent = 0);
    ~LaplacianSharp();
    void execute(cv::Mat *);

private slots:
    void on_btnRun_clicked();


private:
    Ui::LaplacianSharp *ui;

signals:
    void run();
};

#endif // LAPLACIANSHARP_H
