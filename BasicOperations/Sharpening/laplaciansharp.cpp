#include "laplaciansharp.h"
#include "ui_laplaciansharp.h"

LaplacianSharp::LaplacianSharp(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LaplacianSharp)
{
    ui->setupUi(this);
}

LaplacianSharp::~LaplacianSharp()
{
    delete ui;
}

void LaplacianSharp::on_btnRun_clicked()
{
    emit run();
}

void LaplacianSharp::execute(cv::Mat *image)
{
    cv::Mat kernel = (cv::Mat_<float>(3,3) <<
            -1,  -1, -1,
            -1,   9, -1,
            -1,  -1, -1);
    cv::filter2D(*image,*image,image->type(),kernel);
}
