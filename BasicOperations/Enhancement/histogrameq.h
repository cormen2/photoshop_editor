#ifndef HISTOGRAMEQ_H
#define HISTOGRAMEQ_H

#include <QWidget>

namespace Ui {
class HistogramEq;
}

class HistogramEq : public QWidget
{
    Q_OBJECT

public:
    explicit HistogramEq(QWidget *parent = 0);
    ~HistogramEq();

private:
    Ui::HistogramEq *ui;
};

#endif // HISTOGRAMEQ_H
