FORMS += \
    $$PWD/canny.ui \
    $$PWD/sobel.ui \
    $$PWD/laplacian.ui

HEADERS += \
    $$PWD/canny.h \
    $$PWD/sobel.h \
    $$PWD/laplacian.h

SOURCES += \
    $$PWD/canny.cpp \
    $$PWD/sobel.cpp \
    $$PWD/laplacian.cpp
