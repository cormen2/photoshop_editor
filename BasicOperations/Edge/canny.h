#ifndef CANNY_H
#define CANNY_H

#include <QWidget>
#include <QImage>
#include <QMatrix3x3>
#ifndef OPENCVCORE
    #define OPENCVCORE
        #include "opencv2/core/core.hpp"
#endif
#ifndef OPENCVHIGHGUI
    #define OPENCVHIGHGUI
        #include "opencv2/highgui/highgui.hpp"
#endif
#ifndef OPENCVPROC
    #define OPENCVPROC
        #include "opencv2/imgproc/imgproc.hpp"
#endif

namespace Ui {
class Canny;
}

class Canny : public QWidget
{
    Q_OBJECT

public:
    explicit Canny(QWidget *parent = 0);
    ~Canny();

    cv::Mat execute(cv::Mat, int, int);

signals:
    void cannyEcecute(int , int);

private slots:

    void on_horizontalSlider_sliderMoved(int position);

    void on_horizontalSlider_2_sliderMoved(int position);


private:
    Ui::Canny *ui;

    int sliderOneCurrentPosition = 0;
    int sliderTwoCurrentPosition = 0;
};

#endif // CANNY_H
