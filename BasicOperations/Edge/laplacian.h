#ifndef LAPLACIAN_H
#define LAPLACIAN_H

#include <QWidget>
#ifndef OPENCVCORE
    #define OPENCVCORE
        #include "opencv2/core/core.hpp"
#endif
#ifndef OPENCVHIGHGUI
    #define OPENCVHIGHGUI
        #include "opencv2/highgui/highgui.hpp"
#endif
#ifndef OPENCVPROC
    #define OPENCVPROC
        #include "opencv2/imgproc/imgproc.hpp"
#endif

namespace Ui {
class Laplacian;
}

class Laplacian : public QWidget
{
    Q_OBJECT

public:
    explicit Laplacian(QWidget *parent = 0);
    ~Laplacian();

     cv::Mat execute(cv::Mat, int, int);

private slots:

     void on_horizontalSlider_sliderMoved(int position);

     void on_horizontalSlider_2_sliderMoved(int position);

signals:

    void laplacianEcecute(int, int);

private:
    Ui::Laplacian *ui;

    int sliderAlphaCurrentPosition = 0;
    int sliderBetaCurrentPosition = 0;
};

#endif // LAPLACIAN_H
