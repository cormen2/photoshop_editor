#include "sobel.h"
#include "ui_sobel.h"

using namespace cv;
using namespace std;

Sobel::Sobel(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Sobel)
{
    ui->setupUi(this);
}

Sobel::~Sobel()
{
    delete ui;
}


Mat Sobel::execute(Mat src , int slAlpha , int slBeta , int slGamma )
{
    Mat src_gray;

    int ddepth = CV_16S;
    int scale = 1;
    int delta = 0;

    if(slAlpha==0 && slBeta==0 && slGamma==0){

        return src;
    }else{

        /// cv::Mat src_gray;
        Mat gaussian;
        GaussianBlur( src, gaussian, Size(3,3), 0, 0, cv::BORDER_DEFAULT );

        /// Convert it to gray
        cvtColor( gaussian, src_gray, CV_RGB2GRAY );

        /// Generate grad, grad_x and grad_y

        Mat grad, grad_x, grad_y;
        Mat abs_grad_x, abs_grad_y;

        /// Gradient X
        cv::Sobel( src_gray, grad_x, ddepth, 1, 0, 3, scale, delta, cv::BORDER_DEFAULT );
        convertScaleAbs( grad_x, abs_grad_x );

        /// Gradient Y
        cv::Sobel( src_gray, grad_y, ddepth, 0, 1, 3, scale, delta, cv::BORDER_DEFAULT );
        convertScaleAbs( grad_y, abs_grad_y );

        /// Total Gradient (approximate)
        addWeighted( abs_grad_x, slAlpha, abs_grad_y, slBeta, slGamma, grad );

        return grad;
    }

    ////------------------ second approach ----------------------------------------
//         Mat img = imgMat;
//         Mat img1 = img.clone();
//         Mat img2 = img.clone();

//         float sum1;
//         float sum2;

//         float kernel1[3][3] = {
//                                 { -1.0, 0.0, 1.0 },
//                                 { -2.0, 0.0, 2.0 },
//                                 { -1.0, 0.0, 1.0 }
//                             };

//             float kernel2[3][3] = {
//                 { 1.0, 2.0, 1.0 },
//                 { 0.0, 0.0, 0.0 },
//                 { -1.0, -2.0, 1.0 }
//             };

//             //Defining Laplacian filter
//             float kernel3[3][3] = {
//                 {-1.0, -1.0, -1.0},
//                 {-1.0, 8.0, -1.0},
//                 {-1.0, -1.0, -1.0}
//             };

//             //defining the kernel for Gaussian Filter
//             float kernel4[3][3] = {
//                 { 0.01, 0.08, 0.01 },
//                 { 0.08, 0.64, 0.08 },
//                 { 0.01, 0.08, 0.01 }
//             };



//         for (int y = 0; y < img.rows; y++)
//             for (int x = 0; x < img.cols; x++)
//                 img1.at<uchar>(y, x) = 0.0;

//         for (int y = 0; y < img.rows; y++)
//             for (int x = 0; x < img.cols; x++)
//                 img2.at<uchar>(y, x) = 0.0;

//         for (int y = 1; y < img.rows - 1; y++) {
//             for (int x = 1; x < img.cols - 1; x++) {
//                 short x_weight = 0.0;
//                 short y_weight = 0.0;
//                 sum1 = 0.0;
//             for (short i = -1; i <= 1; i++) {
//                 for (short j = -1; j <= 1; j++) {
//                     x_weight += kernel1[i+1][j+1] * img.at<uchar>(y, x);
//                     y_weight += kernel2[i+1][j+1] * img.at<uchar>(y, x);
//                     sum1 += kernel3[i + 1][j + 1] * img.at<uchar>(y, x);
//                     //cout << sum1 << "-" << endl;
//                     sum1 = abs(sum1);
//                 }
//             }
//             //Normalizing the pixel values
//                 short val = abs(x_weight) + abs(y_weight);
//                 if (val > 180)
//                     val = 255;
//                 else if (val <= 180)
//                     val = 0;
//                 img1.at<uchar>(y, x) = (255 - (unsigned char)(val));
//                 if (sum1 > 200)
//                     sum1 = 255;
//                 else if (sum1 <= 50)
//                     sum1 = 0;
//                 img2.at<uchar>(y, x) = (255 - (unsigned char)(sum1));
//                     }
//                 }

//         return img1;

}




void Sobel::on_horizontalSlider_sliderMoved(int position)
{
    sliderAlphaCurrentPosition = position;
    emit sobelEcecute(sliderAlphaCurrentPosition, sliderBetaCurrentPosition, sliderGammaCurrentPosition);

}

void Sobel::on_horizontalSlider_2_sliderMoved(int position)
{
    sliderBetaCurrentPosition = position;
   emit sobelEcecute(sliderAlphaCurrentPosition, sliderBetaCurrentPosition, sliderGammaCurrentPosition);
}

void Sobel::on_horizontalSlider_3_sliderMoved(int position)
{
    sliderGammaCurrentPosition = position;
    emit sobelEcecute(sliderAlphaCurrentPosition, sliderBetaCurrentPosition, sliderGammaCurrentPosition);
}
