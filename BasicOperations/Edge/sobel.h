#ifndef SOBEL_H
#define SOBEL_H

#include <QWidget>

#ifndef OPENCVCORE
    #define OPENCVCORE
        #include "opencv2/core/core.hpp"
#endif
#ifndef OPENCVHIGHGUI
    #define OPENCVHIGHGUI
        #include "opencv2/highgui/highgui.hpp"
#endif
#ifndef OPENCVPROC
    #define OPENCVPROC
        #include "opencv2/imgproc/imgproc.hpp"
#endif

namespace Ui {
class Sobel;
}

class Sobel : public QWidget
{
    Q_OBJECT

public:
    explicit Sobel(QWidget *parent = 0);
    ~Sobel();

    cv::Mat execute(cv::Mat, int , int, int);

signals:
    void sobelEcecute(int, int, int);

private slots:

    void on_horizontalSlider_sliderMoved(int position);

    void on_horizontalSlider_2_sliderMoved(int position);

    void on_horizontalSlider_3_sliderMoved(int position);

private:
    Ui::Sobel *ui;
    int sliderAlphaCurrentPosition = 0;
    int sliderBetaCurrentPosition = 0;
    int sliderGammaCurrentPosition = 0;
};

#endif // SOBEL_H
