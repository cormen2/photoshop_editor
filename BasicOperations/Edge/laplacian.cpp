#include "laplacian.h"
#include "ui_laplacian.h"


using namespace cv;
using namespace std;

Laplacian::Laplacian(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Laplacian)
{
    ui->setupUi(this);
}

Laplacian::~Laplacian()
{
    delete ui;
}

Mat Laplacian::execute(Mat src , int slAlpha, int slBeta)
{
    Mat dst;

    cv::Laplacian(src, dst, CV_32F, 1, slAlpha, slBeta);
    cvtColor( dst, dst, CV_RGB2GRAY );
    convertScaleAbs(dst, dst, slAlpha, slBeta);

    blur( dst, dst, Size(3,3));

    return dst;
}


void Laplacian::on_horizontalSlider_sliderMoved(int position)
{
    sliderAlphaCurrentPosition = position;
    emit laplacianEcecute(sliderAlphaCurrentPosition, sliderBetaCurrentPosition);
}

void Laplacian::on_horizontalSlider_2_sliderMoved(int position)
{

    sliderBetaCurrentPosition = position;
    emit laplacianEcecute(sliderAlphaCurrentPosition, sliderBetaCurrentPosition);
}
