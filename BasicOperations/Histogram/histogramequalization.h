#ifndef HISTOGRAMEQUALIZATION_H
#define HISTOGRAMEQUALIZATION_H

#include <QWidget>
#ifndef OPENCVCORE
    #define OPENCVCORE
        #include "opencv2/core/core.hpp"
#endif
#ifndef OPENCVHIGHGUI
    #define OPENCVHIGHGUI
        #include "opencv2/highgui/highgui.hpp"
#endif
#ifndef OPENCVPROC
    #define OPENCVPROC
        #include "opencv2/imgproc/imgproc.hpp"
#endif

namespace Ui {
class HistogramEqualization;
}

class HistogramEqualization : public QWidget
{
    Q_OBJECT

public:
    explicit HistogramEqualization(QWidget *parent = 0);
    ~HistogramEqualization();

    cv::Mat histEq(cv::Mat);

    long compute_hist_16U (cv::Mat* img, long* hist, bool cumul);

    cv::Mat createHist(cv::Mat &src, int histSize);

signals:
    void histogramEqualizationEcecute();

private slots:
    void on_pushButton_clicked();

private:
    Ui::HistogramEqualization *ui;
};

#endif // HISTOGRAMEQUALIZATION_H
