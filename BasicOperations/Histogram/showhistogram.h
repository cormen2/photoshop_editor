#ifndef SHOWHISTOGRAM_H
#define SHOWHISTOGRAM_H

#include <QWidget>

#ifndef OPENCVCORE
    #define OPENCVCORE
        #include "opencv2/core/core.hpp"
#endif
#ifndef OPENCVHIGHGUI
    #define OPENCVHIGHGUI
        #include "opencv2/highgui/highgui.hpp"
#endif
#ifndef OPENCVPROC
    #define OPENCVPROC
        #include "opencv2/imgproc/imgproc.hpp"
#endif

namespace Ui {
class ShowHistogram;
}

class ShowHistogram : public QWidget
{
    Q_OBJECT

public:
    explicit ShowHistogram(QWidget *parent = 0);
    ~ShowHistogram();

    cv::Mat calculateHistogram(cv::Mat);
    cv::Mat viewHistograms(cv::Mat);

private slots:
    void on_rdRgb_clicked();

    void on_rdXyz_clicked();

    void on_rdYcc_clicked();

    void on_rdHsv_clicked();

    void on_rdLuv_clicked();

    void on_rdHls_clicked();

signals:
    void histogramShowEcecute();

private:
    Ui::ShowHistogram *ui;
};

#endif // SHOWHISTOGRAM_H
