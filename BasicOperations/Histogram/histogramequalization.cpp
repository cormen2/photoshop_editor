#include "histogramequalization.h"
#include "ui_histogramequalization.h"
#include <QDebug>

using namespace cv;
HistogramEqualization::HistogramEqualization(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HistogramEqualization)
{
    ui->setupUi(this);
}

HistogramEqualization::~HistogramEqualization()
{
    delete ui;
}

cv::Mat HistogramEqualization::histEq(cv::Mat src)
{

    Mat dst;
    cv::cvtColor(src,dst,CV_RGB2HSV);

    std::vector<cv::Mat> bgr_planes;
    cv::split( dst, bgr_planes );

    equalizeHist(bgr_planes[0] , bgr_planes[0]);
    equalizeHist(bgr_planes[1] , bgr_planes[1]);
    equalizeHist(bgr_planes[2] , bgr_planes[2]);

    merge(bgr_planes, dst);

    return dst;
}

void HistogramEqualization::on_pushButton_clicked()
{
    emit histogramEqualizationEcecute();
}


//long HistogramEqualization::compute_hist_16U (Mat* img, long* hist, bool cumul)
//{
//    unsigned short i, j, k;
//    long* b;
//    long max = 0;

//    //is the image 16bits grayscale ?
//    assert(img->channels() == 1);
//    assert(CV_16U == img->type());

//    //histogram calculation
//    for(i=0 ; i<img->cols ; i++)
//    {
//        for(j=0 ; j<img->rows ; j++)
//        {
//            hist[img->at<unsigned short>(j,i)]++;
//            if(hist[img->at<unsigned short>(j,i)] > max)
//                max = hist[img->at<unsigned short>(j,i)];
//        }
//    }

//    //Cumulative histogram calculation (if cumul=true)
//    if(cumul)
//    {
//        for(b=hist ; b<hist+65535 ; b++)
//        {
//            *(b+1) += *b;
//        }
//    }
//    return (cumul ? hist[65535] : max);
//}

//Mat HistogramEqualization::createHist(Mat& src, int histSize){

//  /// Set the ranges ( for B,G,R) )
//  float range[] = { 0, 256 } ;
//  const float* histRange = { range };

//  bool uniform = true;
//  bool accumulate = false;

//  Mat outPut;

//  /// Compute the histograms:
//  calcHist( &src, 1, 0, Mat(), outPut, 1, &histSize, &histRange, uniform, accumulate );
//  return outPut;
//}


