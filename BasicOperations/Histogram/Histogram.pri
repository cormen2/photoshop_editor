FORMS += \
    $$PWD/showhistogram.ui \
    $$PWD/histogramequalization.ui

HEADERS += \
    $$PWD/showhistogram.h \
    $$PWD/histogramequalization.h

SOURCES += \
    $$PWD/showhistogram.cpp \
    $$PWD/histogramequalization.cpp
