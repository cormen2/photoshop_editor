#include "gaussian.h"
#include "ui_gaussian.h"

Gaussian::Gaussian(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Gaussian)
{
    ui->setupUi(this);
    ui->grpSigmaX->setLayout(ui->laySigmaX);
    ui->grpSigmaY->setLayout(ui->laySigmaY);
    ui->grpSize->setLayout(ui->laySize);
}

Gaussian::~Gaussian()
{
    delete ui;
}

void Gaussian::execute(cv::Mat *image)
{
    int kernelSize = ui->spnSize->value();
    int sigmaX = ui->sldSigmaX->value();
    int sigmaY = ui->sldSigmaY->value();

    cv::GaussianBlur(*image,*image,cv::Size(kernelSize,kernelSize),sigmaX,sigmaY);
}

void Gaussian::on_btnRun_clicked()
{
    emit run();
}

void Gaussian::on_sldSigmaX_valueChanged(int value)
{
    ui->grpSigmaX->setTitle("Sigma X : " + QString::number(value));
}

void Gaussian::on_sldSigmaY_valueChanged(int value)
{
    ui->grpSigmaY->setTitle("Sigma Y : " + QString::number(value));
}
