#ifndef BILATERAL_H
#define BILATERAL_H

#include <QWidget>
#include <opencv2/imgproc.hpp>
#include <opencv2/core.hpp>

namespace Ui {
class Bilateral;
}

class Bilateral : public QWidget
{
    Q_OBJECT

public:
    explicit Bilateral(QWidget *parent = 0);
    ~Bilateral();
    void execute(cv::Mat*);

private:
    Ui::Bilateral *ui;

signals:
    void run();
private slots:
    void on_btnRun_clicked();
    void on_sldSigmaColor_valueChanged(int value);
    void on_sldSigmaSpace_valueChanged(int value);
};

#endif // BILATERAL_H
