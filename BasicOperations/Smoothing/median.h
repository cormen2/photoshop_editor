#ifndef MEDIAN_H
#define MEDIAN_H

#include <QWidget>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

namespace Ui {
class Median;
}

class Median : public QWidget
{
    Q_OBJECT

public:
    explicit Median(QWidget *parent = 0);
    ~Median();
    void execute(cv::Mat*);

private slots:
    void on_btnRun_clicked();

private:
    Ui::Median *ui;

signals:
    void run();
};

#endif // MEDIAN_H
