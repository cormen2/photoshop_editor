FORMS += \
    $$PWD/gaussian.ui \
    $$PWD/median.ui \
    $$PWD/bilateral.ui

HEADERS += \
    $$PWD/gaussian.h \
    $$PWD/median.h \
    $$PWD/bilateral.h

SOURCES += \
    $$PWD/gaussian.cpp \
    $$PWD/median.cpp \
    $$PWD/bilateral.cpp
