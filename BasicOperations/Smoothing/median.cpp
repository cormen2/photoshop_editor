#include "median.h"
#include "ui_median.h"

Median::Median(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Median)
{
    ui->setupUi(this);
}

Median::~Median()
{
    delete ui;
}
void Median::execute(cv::Mat *image)
{
    int size = ui->spnSize->value();
    cv::medianBlur(*image,*image,size);
}

void Median::on_btnRun_clicked()
{
    emit run();
}
