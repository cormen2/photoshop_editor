#include "bilateral.h"
#include "ui_bilateral.h"

Bilateral::Bilateral(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Bilateral)
{
    ui->setupUi(this);

    ui->grpSigmaColor->setLayout(ui->laySigmaColor);
    ui->grpSigmaSpace->setLayout(ui->laySigmaSpace);
    ui->grpSize->setLayout(ui->laySize);
}

Bilateral::~Bilateral()
{
    delete ui;
}

void Bilateral::execute(cv::Mat *image)
{
    int filterSize = ui->spnSize->value();
    int sigmaColor = ui->sldSigmaColor->value();
    int sigmaSpace = ui->sldSigmaSpace->value();

    cv::Mat out = image->clone();
    cv::bilateralFilter(*image,out,filterSize,sigmaColor,sigmaSpace);
    *image = out;
}

void Bilateral::on_btnRun_clicked()
{
    emit run();
}

void Bilateral::on_sldSigmaColor_valueChanged(int value)
{
    ui->grpSigmaColor->setTitle("Sigma Color : " + QString::number(value));
}

void Bilateral::on_sldSigmaSpace_valueChanged(int value)
{
    ui->grpSigmaSpace->setTitle("Sigma Space : " + QString::number(value));
}
