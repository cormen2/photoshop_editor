#ifndef GAUSSIAN_H
#define GAUSSIAN_H

#include <QWidget>
#include <opencv2/imgproc.hpp>
#include <opencv2/core.hpp>

namespace Ui {
class Gaussian;
}

class Gaussian : public QWidget
{
    Q_OBJECT

public:
    explicit Gaussian(QWidget *parent = 0);
    ~Gaussian();
    void execute(cv::Mat*);
private:
    Ui::Gaussian *ui;
signals:
    void run();
private slots:
    void on_btnRun_clicked();
    void on_sldSigmaX_valueChanged(int value);
    void on_sldSigmaY_valueChanged(int value);
};

#endif // GAUSSIAN_H
