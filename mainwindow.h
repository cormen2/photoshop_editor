#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "maintree.h"
#include "mainsetting.h"
#include "Config/config.h"
#include <QLabel>
#include <QString>
#include <QDebug>
#include <QStylePainter>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <QMainWindow>
#include <QFileDialog>
#include <QtAlgorithms>
#include <opencv2/core.hpp>
#include "BasicOperations/Edge/canny.h"
#include "BasicOperations/Histogram/showhistogram.h"
#include "BasicOperations/Histogram/histogramequalization.h"
#include "BasicOperations/Edge/sobel.h"
#include "BasicOperations/Edge/laplacian.h"
#include "BasicOperations/Morphological/erosion.h"
#include "BasicOperations/Morphological/dilation.h"
#include "BasicOperations/Morphological/advtransformation.h"
#include "BasicOperations/Morphological/thinning.h"
#include "BasicOperations/Sharpening/laplaciansharp.h"
#include "BasicOperations/Smoothing/gaussian.h"
#include "BasicOperations/Smoothing/median.h"
#include "BasicOperations/Smoothing/bilateral.h"
#include "BasicOperations/Edge/canny.h"
#include "CustomWidget/customgraphicsview.h"
#include "BasicOperations/Custom/customfunction.h"


#include <QPair>
#include <QVector>
#include <QMenu>

namespace Ui {
class MainWindow;
}

enum class DockWidgetState
{
    Unknown = -1,
    Docked = 0,    //! DockWidget is docked on MainWindow
    Floating = 1,  //! DockWidget is floating
    Hidden = 2,    //! DockWidget is auto hidden
    Closed = 3,    //! DockWidget is closed by button X
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QLabel      *lblMainImage;
    CustomGraphicsView *graphicScene;
    QGraphicsView *graphicView;
    void imShow(QImage);
     QGraphicsPixmapItem *item;


private:
    Ui::MainWindow *ui;
    MainTree *mainTree;
    MainSetting *mainSetting;
    Canny *canny;
    Sobel *sobel;
    Laplacian *laplacian;
    QImage mainImage;
    Erosion *er;
    Dilation *di;
    ADVTransformation *advt;
    Thinning *thin;
    ShowHistogram *histShow;
    HistogramEqualization *histEqualization;
    boolean isCropClicked = false;
    LaplacianSharp *lapSh;
    Gaussian *gauss;
    Median *med;
    Bilateral *bilat;
    CustomFunction *custom;

public slots:
    void test(int i);
    void treeSignalReciever(QTreeWidgetItem*  ,int);
    void cannyExecuteReciever(int , int);
    void sobelExecuteReciever(int, int, int);
    void laplacianExecuteReciever(int, int);
    void histogramEqualizationExecuteReciever();
    cv::Mat     QImage2Mat(QImage const& src);
    QImage      Mat2QImage(cv::Mat const& src);
    void erodeExecuteReciever();
    void dilateExecuteReciever();
    void AdvTExecuteReciever();
    void thiningExecuteReciever();
    void showHistogramExecuteReciever();
    void LaplacianSharpExecuteReciever();
    void GaussianExecuteReciever();
    void MedianExecuteReciever();
    void BilateralExecuteReciever();
    void CustomeFuncExecuteReciever();
    void on_actionOpen_triggered();
    void cropReciever(QAction *);

private slots:
    void on_actionReset_Image_triggered();
    void on_actionCrop_triggered();
    void on_label_main_clicked();
    void on_actionDone_triggered();
};

#endif // MAINWINDOW_H
