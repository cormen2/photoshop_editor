#ifndef CONFIG_H
#define CONFIG_H

enum methods{

       CANNY,
       SOBEL,
       LAPLACIAN,
       LAPLACIAN_SHARP,
       EROSION,
       DILATION,
       ADVTRANS,
       THINING,
       CUSTOM,
       THIKENING,
       GUSSIAN,
       MEDIAN,
       BILATERAL,
       MEAN,
       HISTOGRAMEQ,
       LOGTRANSFER,
       NEGATIVE,
       ROTATE,
       RESIZE,
       FLIP,
       DISTORTION,
       SHOW_HISTOGRAM,
    HISTOGRAM_EQUALIZATION

   };

#endif // CONFIG_H
