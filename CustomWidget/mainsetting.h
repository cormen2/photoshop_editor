#ifndef MAINSETTING_H
#define MAINSETTING_H
#include <QtCore>
#include <QtGui>
#include <QWidget>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QDockWidget>

class MainSetting : public QWidget
{
    Q_OBJECT

public:
    MainSetting();
    QDockWidget *doc;
    QWidget *widget;
    QVBoxLayout *mainVLayout;

signals:

public slots:
};

#endif // MAINSETTING_H
