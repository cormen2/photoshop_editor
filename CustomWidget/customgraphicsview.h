#ifndef CUSTOMGRAPHICSVIEW_H
#define CUSTOMGRAPHICSVIEW_H

#include <QGraphicsScene>
#include <QtCore>
#include <QtGui>
#include <QObject>
#include <QGraphicsView>
#include <QGraphicsTextItem>
#include <QDebug>
#include <QMenu>


class CustomGraphicsView : public QGraphicsScene
{
    Q_OBJECT
public:
    CustomGraphicsView(QObject *parent = 0);
    void clear();
    void clearRects();
    bool over;
    bool cropIsSelected;
    bool cropRectIsDrawed;
    QString str;
    QGraphicsTextItem cursor;
    int startX;
    int startY;
    int endX;
    int endY;
    QPointF endPoint;
    QPainter *myPainter;
    QMenu *menu;

public slots:
    void drawBackground(QPainter *painter, const QRectF &rect);
    void mouseMoveEvent(QGraphicsSceneMouseEvent * mouseEvent);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);


};

#endif // CUSTOMGRAPHICSVIEW_H
