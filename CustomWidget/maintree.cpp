#include "maintree.h"
#include "mainwindow.h"

MainTree::MainTree(QMainWindow *qMainWindow):
    QWidget()
{
    tree = new QTreeWidget;

    init();

    doc = new QDockWidget;
    doc->setWindowTitle("Functions");

    QVBoxLayout *mainVLayout = new QVBoxLayout();
    QGroupBox *mainGroupBox = new QGroupBox();

    mainGroupBox->setLayout(mainVLayout);

    doc->setWidget(mainGroupBox);
    mainVLayout->addWidget(tree);

    connect(tree,SIGNAL(itemClicked(QTreeWidgetItem*,int)), qMainWindow, SLOT(treeSignalReciever(QTreeWidgetItem*,int)));
}


void MainTree::init()
{
    tree->setColumnCount(2);
    tree->hideColumn(1);
    tree->setHeaderLabel("");

    QTreeWidgetItem *item2 = new QTreeWidgetItem;
    addRoot(item2 ,"edge");
    addChild(item2,"canny",CANNY);
    addChild(item2,"sobel",SOBEL);

    QTreeWidgetItem *histogram = new QTreeWidgetItem;
    addRoot(histogram ,"histogram");
    addChild(histogram,"shdow histogram",SHOW_HISTOGRAM);
    addChild(histogram,"histogram equalization",HISTOGRAM_EQUALIZATION);

    QTreeWidgetItem *Morphological = new QTreeWidgetItem;
    addRoot(Morphological ,"Morphology");
    addChild(Morphological,"Erosion",EROSION);
    addChild(Morphological,"Dilation",DILATION);
    addChild(Morphological,"Advanced Transformation",ADVTRANS);
    addChild(Morphological,"Thinning",THINING);
    addChild(item2,"laplacian",LAPLACIAN);

    QTreeWidgetItem *Sharpening = new QTreeWidgetItem;
    addRoot(Sharpening ,"Sharpening");
    addChild(Sharpening,"Laplacian",LAPLACIAN_SHARP);

    QTreeWidgetItem *Smoothing = new QTreeWidgetItem;
    addRoot(Smoothing ,"Smoothing");
    addChild(Smoothing,"Gaussian",GUSSIAN);
    addChild(Smoothing,"Median",MEDIAN);
    addChild(Smoothing,"Bilateral",BILATERAL);

    QTreeWidgetItem *Custom = new QTreeWidgetItem;
    addRoot(Custom ,"Custom");
    addChild(Custom,"Custom Function",CUSTOM);

}

void MainTree::addRoot(QTreeWidgetItem *item, QString name)
{
    item->setText(0,name);
    item->setText(1,"-1");
    item->setTextAlignment(0,1);
    tree->addTopLevelItem(item);
}

void MainTree::addChild(QTreeWidgetItem *parent, QString name, int code)
{
    QTreeWidgetItem *item = new QTreeWidgetItem();
    item->setText(0,name);
    item->setText(1,QString::number(code));
    item->setTextAlignment(0,1);
    parent->addChild(item);
}
