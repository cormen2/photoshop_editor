#include "customgraphicsview.h"
#define _alto  300
#define _ancho 700
#include <QGraphicsSceneMouseEvent>


CustomGraphicsView::CustomGraphicsView(QObject *parent):QGraphicsScene(parent)
{ // Constructor of Scene
    this->over = false;
}

void CustomGraphicsView::drawBackground(QPainter *painter, const QRectF &rect)
{

    myPainter = painter;

}

void CustomGraphicsView::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{

    this->update();

        endX = event->scenePos().x();
        endY = event->scenePos().y();

        if(cropIsSelected){

            const QPointF topLeft(startX, startY);
            const QPointF bottomRight(endX, endY);

            QRectF exactRect(topLeft , bottomRight);
            QPen pen = QPen(Qt::red, 2, Qt::DashLine);
            this->clearRects();
            QGraphicsItem *myItem = addRect(exactRect, pen, QBrush(Qt::transparent));
            this->addItem(myItem);

            cropRectIsDrawed = true;

        }

}

void CustomGraphicsView::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    this->update();

    if(event->button()==Qt::RightButton ){

        if(cropRectIsDrawed){

          QAction action1("crop", this);
          action1.setData(1);

          QAction action2("cancel", this);
          action2.setData(2);

          menu->addAction(&action1);
          menu->addAction(&action2);

          menu->exec(event->screenPos());
        }
    }

    double rad = 1;
    startX = event->scenePos().x();
    startY = event->scenePos().y();

}


void CustomGraphicsView::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    this->update();

     if(event->button()==Qt::LeftButton ){

         endX = event->scenePos().x();
         endY = event->scenePos().y();

         if(cropIsSelected){

             const QPointF topLeft(startX, startY);
             const QPointF bottomRight(endX, endY);

             QRectF exactRect(topLeft , bottomRight);
             QPen pen(Qt::red, 2, Qt::DashLine);
             QGraphicsItem *myItem = addRect(exactRect , pen, QBrush(Qt::transparent));
             this->addItem(myItem);

             cropRectIsDrawed = true;

         }

     }

}


void CustomGraphicsView::clear()
{
    bool itemDeleted = true;
    QList <QGraphicsItem*> itemList = items();

    while (itemDeleted && !itemList.isEmpty()) {
        int numItems = itemList.size();
        int iItem;
        itemDeleted = false;
        for (iItem = 0; (iItem < numItems) && !itemDeleted; iItem++) {
            QGraphicsItem *item = itemList.at(iItem);
            if (item->parentItem() == NULL) {
                removeItem(item);
                delete item;
                itemDeleted = true;
            }
        }
        itemList = items();
    }
    if (!itemList.isEmpty() && !itemDeleted)
        qWarning("Problem in MyGraphicsScene::clear()");
}

void CustomGraphicsView::clearRects()
{
    bool itemDeleted = true;
    QList <QGraphicsItem*> itemList = items();

    while (itemDeleted && !itemList.isEmpty()) {
        int numItems = itemList.size();
        int iItem;
        itemDeleted = false;
        for (iItem = 0; (iItem < numItems-1) && !itemDeleted; iItem++) {
            QGraphicsItem *item = itemList.at(iItem);
            if (item->parentItem() == NULL) {
                removeItem(item);
                delete item;
                itemDeleted = true;
            }
        }
        itemList = items();
    }
    if (!itemList.isEmpty() && !itemDeleted)
        qWarning("Problem in MyGraphicsScene::clear()");
}








